#1
a = zeros(10, 10);
a(1, :) = 1;

#2
a = [ones(1, 10); zeros(9, 10)];

disp(a)